package main

import (
	"fmt"
	"runtime"

	"gitee.com/lorock/color"
	// "gitee.com/lorock/goutil/dump"
)

func main() {
	fmt.Println("OS", runtime.GOOS)

	fmt.Println("IsSupport256Color", color.IsSupport256Color())
	fmt.Println("IsSupportColor", color.IsSupportColor())

	// dump.P(os.Environ())
}
