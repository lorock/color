package main

import (
	"fmt"
	"os"

	"gitee.com/lorock/color"
)

// go run _examples/ref/cmd_color1.go
func main() {
	color.ForceOpenColor()

	ss := os.Environ()
	for _, v := range ss {
		fmt.Println(v)
	}

	str := color.Question.Render("msg", "More")
	fmt.Println(str)
}
