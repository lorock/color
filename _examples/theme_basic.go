package main

import (
	"fmt"

	"gitee.com/lorock/color"
)

// go run _examples/theme_basic.go
func main() {
	fmt.Println("Built In Themes(styles):")
	fmt.Println("------------------ BASIC STYLE ------------------")

	for name, s := range color.Themes {
		s.Println(name, "message")
	}
}
